import 'package:flutter/material.dart';

import '../controllers/global_settings.bloc.dart';
import 'default_pattern.bloc.dart';

class ObjectsPattern {
  double _proportionXY;
  BuildContext _context;
  MediaQueryData _deviceInfo;
  var defPat = MyDefaultPattern();
  GlobalSettings gblSet = GlobalSettings();

  // Get context
  getContext(BuildContext context) {
    _context = context;
    _deviceInfo = MediaQuery.of(_context);
    _proportionXY = gblSet.heightDefault / gblSet.widthDefault;

    setSizes();
  }

  // Get general sizes
  setSizes() {
    double _heightFragProportion = 0.184;
    double _heightFragNavButProp = 0.12;

    gblSet.scafHeight = (_deviceInfo.size.height).toInt().toDouble();
    gblSet.scafWidth = (_deviceInfo.size.width).toInt().toDouble();

    if (gblSet.scafHeight / gblSet.scafWidth < _proportionXY) {
      gblSet.scafWidth = gblSet.scafHeight / _proportionXY;
      print('scafWidth ${gblSet.scafWidth}\n---');
    }

    gblSet.topBarHeight =
        gblSet.round(gblSet.scafHeight * _heightFragProportion); // 152 Default
    gblSet.topBarWidth = gblSet.scafWidth;

    gblSet.fragWidth = gblSet.scafWidth;
    gblSet.fragHeight = gblSet.scafHeight - gblSet.topBarHeight;

    print('---Measures---');
    print('scafHeight ${gblSet.scafHeight}');
    print('scafWidth ${gblSet.scafWidth}\n---');
    print('topBarHeight ${gblSet.topBarHeight}');
    print('topBarWidth ${gblSet.topBarWidth}\n---');
    print('fragHeight ${gblSet.fragHeight}');
    print('fragWidth ${gblSet.fragWidth}');
    print('---Measures---');

    gblSet.papyrusBottom = gblSet.fragHeight * (1 - _heightFragNavButProp);

    //
    gblSet.ftsNavBar = gblSet.sizeH(15);
    gblSet.ftsNormal = gblSet.sizeH(18);
    gblSet.ftsTitle = gblSet.sizeH(26);
    //
    gblSet.bannerNameH = gblSet.topBarHeight * 0.25;
    gblSet.bannerNameW = gblSet.sizeW(260);
    gblSet.sealH = gblSet.sizeH(60);
    gblSet.sealW = gblSet.sizeW(60);
  }
}
