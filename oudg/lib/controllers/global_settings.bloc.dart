import 'default_pattern.bloc.dart';

/// Necessário executar MyGlobalContext().getContext() para obter os tamanhos do contexto
class GlobalSettings {
  GlobalSettings._privateConstructor();

  static final GlobalSettings _instance = GlobalSettings._privateConstructor();

  var defPat = MyDefaultPattern();

  double heightDefault = 846;
  double widthDefault = 411;

  // General sizes
  /// Especial
  double scafHeight;

  /// Especial
  double scafWidth;

  /// 80% scafHeight
  double fragHeight;

  /// same as scafWidth
  double fragWidth;

  /// 20% scafHeight
  double topBarHeight;

  /// same as scafWidth
  double topBarWidth;

  /// 20% fragHeight
  double papyrusBottom;

  // Font sizes
  /// base 13
  double ftsNavBar;

  /// base 20
  double ftsTitle;

  /// base 15
  double ftsNormal;

  // Widgets sizes

  /// base 40
  double bannerNameH;

  /// base 260
  double bannerNameW;

  /// base 40
  double sealH;

  /// base 40
  double sealW;

  factory GlobalSettings() {
    return _instance;
  }

  double round(double num, [int precision = 0]) {
    String ret = num.toStringAsFixed(precision);
    return double.parse(ret);
  }

  double sizeH(double size, [int precision = 0]) {
    return round(scafHeight / round(heightDefault / size), precision);
  }

  double sizeW(double size, [int precision = 0]) {
    return round(scafWidth / round(widthDefault / size), precision);
  }
}
