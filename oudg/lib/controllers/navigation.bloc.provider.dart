import 'dart:async';

import 'package:flutter/material.dart';

class MyNavigation {
  BuildContext _context;
  MediaQueryData _deviceInfo;
  //
  int currentIndex = 0;
  int _total = -1;
  int get total => _total;
  var namePage = List<String>();
  //
  final _blocController = StreamController<int>();
  Stream<int> get myStream => _blocController.stream;
  var fragments = List<Widget>();

  // Get context
  getContext(BuildContext context) {
    _context = context;
    _deviceInfo = MediaQuery.of(_context);
  }

  Stream getStream() {
    return myStream;
  }

  closeStream() {
    _blocController.close();
// @override
// void dispose() {
//   var navFrag = Provider.of<MyNavigation>(context);
//   navFrag.fecharStream;
//   super.dispose();
// }
  }

  int addFragment(Widget myWidget, String name) {
    fragments.add(myWidget);
    namePage.add(name);
    _total++;
    return _total;
  }

  int getPage(String name) {
    return namePage.indexOf(name);
  }

  Widget getCurrentFragment() {
    return fragments[currentIndex];
  }

  void navTo(int index) {
    if (index < 0 || index > fragments.length) {
      print("Index error ${index}. namePage Error. ");
    } else {
      currentIndex = index;
      _blocController.sink.add(total);
    }
  }
}
