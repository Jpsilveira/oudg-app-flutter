class MyDefaultPattern {
  // Assets
  final String assBack = "assets/back.png";
  final String assBgCoins = "assets/bg_coins.png";
  final String assBgMain = "assets/bg_main.png";
  final String assBgMenu = "assets/bg_menu.png";
  final String assBgMenuOff = "assets/bg_menu_off.png";
  final String assBgName = "assets/bg_name.png";
  final String assForward = "assets/forward.png";
  final String assIconLauncher = "assets/ic_launcher.png";
  final String assLogo = "assets/logo.png";
  final String assPapyrus = "assets/papyrus.png";
  final String assSelecOff = "assets/selection_off.png";
  final String assSelecOn = "assets/selection_on.png";

  final String assSealNone_ = "assets/selo_none.png";
  final String assSealAvant = "assets/selo_avantes.png";
  final String assSealBiont = "assets/selo_biontes.png";
  final String assSealGuard = "assets/selo_guardioes.png";
  final String assSealMenta = "assets/selo_mentales.png";
  final String assSplash = "assets/splash.png";

  // final int cstCasteNone_ = 0;
  // final int cstCasteAvant = 1;
  // final int cstCasteBiont = 2;
  // final int cstCasteGuard = 3;
  // final int cstCasteMenta = 4;

  final String cstCasteNone_ = "NON";
  final String cstCasteAvant = "AVT";
  final String cstCasteBiont = "BIO";
  final String cstCasteGuard = "GRD";
  final String cstCasteMenta = "MTL";

  final int aliLeft = 0;
  final int aliRight = 1;
}
