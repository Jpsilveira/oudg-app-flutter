import 'dart:async';

import 'package:flutter/material.dart';

import 'default_pattern.bloc.dart';

class MyGlobalContext {
  BuildContext context;
  var defPat = MyDefaultPattern();

  BuildContext _context;
  MediaQueryData _deviceInfo;

  //
  int _total = -1;

  int get total => _total;

  //
  final _blocController = StreamController<int>();

  Stream<int> get myStream => _blocController.stream;

  //
  String _userName = "João Paulo Silveira";
  String _userNameTribal;
  String _userCaste;

  // Get context
  getContext(BuildContext context) {
    _context = context;
    _deviceInfo = MediaQuery.of(_context);
  }

  Stream getStream() {
    return myStream;
  }

  closeStream() {
    _blocController.close();
  }

  void setUserName(String name) {
    _userName = name;
    _blocController.sink.add(total);
  }

  String getUserName() {
    return _userName;
  }

  void setUserNameTribal(String name) {
    _userNameTribal = name;
    _blocController.sink.add(total);
  }

  String getUserNameTribal() {
    return _userNameTribal;
  }

  void setUserCaste(String caste) {
    _userCaste = caste;
    _blocController.sink.add(total);
  }

  String getUserCaste() {
    if (_userCaste == null) {
      _userCaste = defPat.cstCasteNone_;
    }
    return _userCaste;
  }

  String getUserCasteSeal() {
    String seal = defPat.assSealNone_;
    if (_userCaste == defPat.cstCasteAvant) {
      seal = defPat.assSealAvant;
    } else if (_userCaste == defPat.cstCasteBiont) {
      seal = defPat.assSealBiont;
    } else if (_userCaste == defPat.cstCasteGuard) {
      seal = defPat.assSealGuard;
    } else if (_userCaste == defPat.cstCasteMenta) {
      seal = defPat.assSealMenta;
    }

    return seal;
  }
}
