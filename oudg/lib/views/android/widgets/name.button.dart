import 'package:flutter/material.dart';
import 'package:oudg_flutter/controllers/default_pattern.bloc.dart';
import 'package:oudg_flutter/controllers/global_context.bloc.dart';
import 'package:oudg_flutter/controllers/global_settings.bloc.dart';
import 'package:provider/provider.dart';

class MyNameButton extends StatefulWidget {
  @override
  _MyNameButtonState createState() => _MyNameButtonState();
}

class _MyNameButtonState extends State<MyNameButton> {
  @override
  Widget build(BuildContext context) {
    var defPat = MyDefaultPattern();
    var gloCont = Provider.of<MyGlobalContext>(context);
    GlobalSettings gblSet = GlobalSettings();

    return Expanded(
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(defPat.assBgName),
            fit: BoxFit.fill,
          ),
        ),
        margin: EdgeInsets.only(
          right: gblSet.sizeW(10),
          left: gblSet.sizeW(15),
        ),
        height: gblSet.bannerNameH,
        width: gblSet.bannerNameW,
        alignment: Alignment(0.0, gblSet.sizeW(0.8, 1) * -1),
        child: FlatButton(
          child: Text(
            gloCont.getUserName(),
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: gblSet.ftsNormal,
            ),
          ),
          onPressed: () {},
        ),
      ),
    );
  }
}
