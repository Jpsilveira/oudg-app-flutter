import 'package:flutter/material.dart';
import 'package:oudg_flutter/controllers/global_context.bloc.dart';
import 'package:oudg_flutter/controllers/navigation.bloc.provider.dart';
import 'package:oudg_flutter/views/android/pages/home.view.dart';
import 'package:oudg_flutter/views/android/pages/splash.view.dart';
import 'package:provider/provider.dart';

class MyAndroidApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<MyNavigation>.value(
          value: MyNavigation(),
        ),
        Provider<MyGlobalContext>.value(
          value: MyGlobalContext(),
        ),
      ],
      child: MaterialApp(
        title: "O Último dos Guardiões",
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.orange,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          fontFamily: 'Papyrus',
        ),
        home: MySplash(
          myHome: HomeView(),
        ),
      ),
    );
  }
}
