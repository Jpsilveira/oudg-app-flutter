import 'package:flutter/material.dart';
import 'package:oudg_flutter/controllers/global_context.bloc.dart';
import 'package:oudg_flutter/controllers/global_settings.bloc.dart';
import 'package:provider/provider.dart';

class MySeal extends StatefulWidget {
  @override
  _MySealState createState() => _MySealState();
}

class _MySealState extends State<MySeal> {
  @override
  Widget build(BuildContext context) {
    var gloCont = Provider.of<MyGlobalContext>(context);
    GlobalSettings gblSet = GlobalSettings();

    return Container(
      margin: EdgeInsets.only(
        right: gblSet.sizeW(10),
        bottom: gblSet.sizeH(5),
      ),
      height: gblSet.topBarHeight * 0.25,
      width: gblSet.sealW,
      child: FlatButton(
        padding: EdgeInsets.zero,
        child: Image.asset(
          gloCont.getUserCasteSeal(),
        ),
        onPressed: () {},
      ),
    );
  }
}
