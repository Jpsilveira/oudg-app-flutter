import 'package:flutter/material.dart';
import 'package:oudg_flutter/controllers/default_pattern.bloc.dart';
import 'package:oudg_flutter/controllers/global_settings.bloc.dart';
import 'package:oudg_flutter/views/android/widgets/seal.button.dart';

import 'name.button.dart';

class MyTopBarView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var defPat = MyDefaultPattern();
    GlobalSettings gblSet = GlobalSettings();

    return SizedBox(
      child: Column(
        children: [
          Row(
            children: [
              // Logo image
              Container(
                margin: EdgeInsets.only(
                  left: gblSet.sizeW(10),
                  top: gblSet.sizeH(12),
                ),
                child: Image.asset(
                  defPat.assSplash,
                  alignment: Alignment.center,
                ),
                height: gblSet.topBarHeight * 0.75,
                width: gblSet.sizeW(90),
              ),
              // Title, name and seal
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Space before row
                    Container(
                      height: gblSet.topBarHeight * 0.48,
                      margin: EdgeInsets.only(
                        right: gblSet.sizeH(10),
                        top: gblSet.sizeH(5),
                      ),
                      child: Image.asset(
                        defPat.assLogo,
                      ),
                    ),
                    // Row of the Name and the Seal
                    Row(children: [
                      // Name
                      MyNameButton(),
                      // Seal
                      MySeal(),
                    ]),
                    // Space after title, name and seal
                    SizedBox(
                      height: gblSet.sizeH(5),
                    ),
                  ],
                ),
              ),
              // Space after all
              SizedBox(
                width: gblSet.sizeW(5),
                height: gblSet.sizeH(5),
              ),
            ],
          ),
          // Separate bar
          Image.asset(
            defPat.assBgCoins,
            alignment: Alignment.topCenter,
            fit: BoxFit.fill,
            height: gblSet.sizeH(25),
            width: gblSet.scafWidth,
          ),
        ],
      ),
    );
  }
}
