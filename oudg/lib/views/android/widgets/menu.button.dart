import 'package:flutter/material.dart';
import 'package:oudg_flutter/controllers/global_settings.bloc.dart';

class MyMenuButton extends StatelessWidget {
  final String textButton;
  final String assetButton;
  final Function func;

  MyMenuButton({
    Key key,
    this.textButton,
    this.assetButton,
    this.func,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    GlobalSettings gblSet = GlobalSettings();

    return FlatButton(
      padding: EdgeInsets.only(
        bottom: gblSet.sizeH(5),
      ),
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(assetButton),
            fit: BoxFit.fill,
          ),
        ),
        height: gblSet.sizeH(85),
        alignment: Alignment(
          gblSet.sizeW(0.1, 1),
          gblSet.sizeH(0.2, 1),
        ),
        child: Text(
          textButton,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: gblSet.ftsNormal,
          ),
        ),
      ),
      onPressed: func,
    );
  }
}
