import 'package:flutter/material.dart';
import 'package:oudg_flutter/controllers/default_pattern.bloc.dart';
import 'package:oudg_flutter/controllers/global_settings.bloc.dart';

class MyQuestButton extends StatelessWidget {
  final int textAlign;
  final String textButton;
  final String assetButtonOn;
  final String assetButtonOff;
  bool onOff;
  final Function func;

  MyQuestButton({
    Key key,
    @required this.textAlign,
    @required this.textButton,
    @required this.assetButtonOn,
    @required this.assetButtonOff,
    @required this.onOff,
    @required this.func,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var defPat = MyDefaultPattern();
    GlobalSettings gblSet = GlobalSettings();

    return FlatButton(
      // padding: EdgeInsets.only(
      //     left: textAlign == defPat.aliRight ? gblSet.sizeW(10) : 0,
      //     right: textAlign == defPat.aliLeft ? gblSet.sizeW(10) : 0),
      child: Row(
        children: [
          textAlign == defPat.aliLeft
              ? Text(
                  textButton,
                  style: TextStyle(
                    fontSize: gblSet.ftsNavBar,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                )
              : SizedBox(),
          Container(
            padding: EdgeInsets.only(
                left: textAlign == defPat.aliLeft ? gblSet.sizeW(10) : 0,
                right: textAlign == defPat.aliRight ? gblSet.sizeW(10) : 0),
            child: SizedBox(
              child: Image.asset(onOff ? assetButtonOn : assetButtonOff),
              width: gblSet.sizeW(60),
              height: gblSet.sizeH(60),
            ),
          ),
          textAlign == defPat.aliRight
              ? Expanded(
                  child: Text(
                    textButton,
                    style: TextStyle(
                      fontSize: gblSet.ftsNavBar,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.justify,
                  ),
                )
              : SizedBox(),
        ],
      ),
      onPressed: () {
        // print(onOff);
        func();
      },
    );
  }
}
