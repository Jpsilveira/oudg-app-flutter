import 'package:flutter/material.dart';
import 'package:oudg_flutter/controllers/default_pattern.bloc.dart';
import 'package:oudg_flutter/controllers/global_settings.bloc.dart';

class MyNavButton extends StatelessWidget {
  final int textAlign;
  final String textButton;
  final String assetButton;
  final Function func;

  const MyNavButton({
    Key key,
    this.textAlign,
    this.textButton,
    this.assetButton,
    this.func,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var defPat = MyDefaultPattern();
    GlobalSettings gblSet = GlobalSettings();

    return FlatButton(
      padding: EdgeInsets.only(
          left: textAlign == defPat.aliRight ? gblSet.sizeW(10) : 0,
          right: textAlign == defPat.aliLeft ? gblSet.sizeW(10) : 0),
      child: Row(
        children: [
          textAlign == defPat.aliLeft
              ? Row(
                  children: [
                    SizedBox(
                      width: gblSet.sizeW(10),
                    ),
                    Text(
                      textButton,
                      style: TextStyle(
                        fontSize: gblSet.ftsNavBar,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                )
              : SizedBox(),
          Container(
            padding: EdgeInsets.zero,
            child: SizedBox(
              child: Image.asset(assetButton),
              width: gblSet.sizeW(73),
              height: gblSet.sizeH(73),
            ),
          ),
          textAlign == defPat.aliRight
              ? Row(
                  children: [
                    Text(
                      textButton,
                      style: TextStyle(
                        fontSize: gblSet.ftsNavBar,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      width: gblSet.sizeW(10),
                    ),
                  ],
                )
              : SizedBox(),
        ],
      ),
      onPressed: func,
    );
  }
}
