import 'package:flutter/material.dart';
import 'package:oudg_flutter/controllers/default_pattern.bloc.dart';
import 'package:oudg_flutter/controllers/global_settings.bloc.dart';
import 'package:oudg_flutter/controllers/navigation.bloc.provider.dart';
import 'package:provider/provider.dart';

import '../widgets/navigation.button.dart';

class MyRecord extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var defPat = MyDefaultPattern();
    GlobalSettings gblSet = GlobalSettings();
    var navFrag = Provider.of<MyNavigation>(context);
    var myTitle = "Registro de Nascimento";

    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            left: gblSet.sizeW(10),
            right: gblSet.sizeW(10),
          ),
          width: gblSet.scafWidth,
          height: gblSet.papyrusBottom,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(defPat.assPapyrus),
              fit: BoxFit.fill,
            ),
          ),
          child: Container(
            height: gblSet.papyrusBottom,
            padding: EdgeInsets.all(gblSet.sizeH(20)),
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                Text(
                  myTitle,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: gblSet.ftsTitle,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            MyNavButton(
              textAlign: defPat.aliRight,
              textButton: "Voltar ao Menu",
              assetButton: defPat.assBack,
              func: () => {navFrag.navTo(navFrag.getPage('pagMenu'))},
            )
          ],
        ),
      ],
    );
  }
}
