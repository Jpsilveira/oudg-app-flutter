import 'package:flutter/material.dart';
import 'package:oudg_flutter/controllers/default_pattern.bloc.dart';
import 'package:oudg_flutter/controllers/global_settings.bloc.dart';
import 'package:oudg_flutter/controllers/objects_pattern.bloc.dart';

class MySplash extends StatefulWidget {
  Widget myHome;

  MySplash({
    Key key,
    this.myHome,
  }) : super(key: key);

  @override
  _MySplashState createState() => _MySplashState();
}

class _MySplashState extends State<MySplash> {
  @override
  void initState() {
    super.initState();
    new Future.delayed(
      const Duration(seconds: 3),
      () => Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => widget.myHome),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var defPat = MyDefaultPattern();
    var objPat = ObjectsPattern();
    objPat.getContext(context);
    GlobalSettings gblSet = GlobalSettings();

    return Scaffold(
      backgroundColor: Color(0xFF38056B),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              defPat.assLogo,
              fit: BoxFit.cover,
              repeat: ImageRepeat.noRepeat,
              width: gblSet.scafWidth * 0.8,
            ),
            Image.asset(
              defPat.assSplash,
              fit: BoxFit.cover,
              repeat: ImageRepeat.noRepeat,
              width: gblSet.scafWidth * 0.8,
            ),
          ],
        ),
      ),
    );
  }
}
