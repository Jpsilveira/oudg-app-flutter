import 'package:flutter/material.dart';
import 'package:oudg_flutter/controllers/global_settings.bloc.dart';
import 'package:oudg_flutter/questions/questions.dart';

class MyQuiz extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    GlobalSettings gblSet = GlobalSettings();

    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            left: gblSet.sizeW(10),
            right: gblSet.sizeW(10),
          ),
          width: gblSet.fragWidth,
          height: gblSet.fragHeight,
          child: Questions(
            quizNumber: 2,
          ),
        ),
      ],
    );
  }
}
