import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:oudg_flutter/controllers/default_pattern.bloc.dart';
import 'package:oudg_flutter/controllers/global_context.bloc.dart';
import 'package:oudg_flutter/controllers/global_settings.bloc.dart';
import 'package:oudg_flutter/controllers/navigation.bloc.provider.dart';
import 'package:oudg_flutter/controllers/objects_pattern.bloc.dart';
import 'package:oudg_flutter/views/android/pages/quiz.view.dart';
import 'package:oudg_flutter/views/android/pages/record.view.dart';
import 'package:oudg_flutter/views/android/pages/rpg.view.dart';
import 'package:oudg_flutter/views/android/pages/tavern.view.dart';
import 'package:oudg_flutter/views/android/pages/tribal.view.dart';
import 'package:oudg_flutter/views/android/widgets/top_bar.view.dart';
import 'package:provider/provider.dart';

import 'caste.view.dart';
import 'menu.view.dart';
import 'store.view.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setPreferredOrientations(
      [
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var defPat = MyDefaultPattern();
    var objPat = ObjectsPattern();
    var navFrag = Provider.of<MyNavigation>(context);
    var gloCont = Provider.of<MyGlobalContext>(context);

    objPat.getContext(context);
    navFrag.getContext(context);
    GlobalSettings gblSet = GlobalSettings();

    navFrag.addFragment(MyMenu(), 'pagMenu');
    navFrag.addFragment(MyCaste(), 'pagCaste');
    navFrag.addFragment(MyTribal(), 'pagTribal');
    navFrag.addFragment(MyRecord(), 'pagRecord');
    navFrag.addFragment(MyQuiz(), 'pagQuiz');
    navFrag.addFragment(MyTavern(), 'pagTavern');
    navFrag.addFragment(MyRPG(), 'pagRPG');
    navFrag.addFragment(MyStore(), 'pagStore');

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Container(
          height: gblSet.scafHeight,
          width: gblSet.scafWidth,
          padding: EdgeInsets.all(0),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(defPat.assBgMain),
              fit: BoxFit.fill,
            ),
          ),
          child: Column(
            children: <Widget>[
              Container(
                // TopBar
                height: gblSet.topBarHeight,
                child: StreamBuilder<int>(
                  stream: gloCont.getStream(),
                  initialData: 0,
                  builder: (context, snapshot) {
                    if (snapshot.hasError) {
                      return Text('There is a Stream error. # gloCont');
                    } else {
                      return MyTopBarView();
                    }
                  },
                ),
              ),
              Container(
                height: gblSet.fragHeight,
                child: StreamBuilder<int>(
                  stream: navFrag.getStream(),
                  initialData: 0,
                  builder: (context, snapshot) {
                    if (snapshot.hasError) {
                      return Text('There is a Stream error. # navFrag');
                    } else {
                      return navFrag.getCurrentFragment();
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

//----
//   @override
//   void dispose() {
//     var gblSet = Provider.of<ObjectsPattern>(context);
//     gblSet.fecharStream;
//     super.dispose();
//   }
//----

}
