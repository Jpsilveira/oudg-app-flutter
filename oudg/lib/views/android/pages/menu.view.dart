import 'package:flutter/material.dart';
import 'package:oudg_flutter/controllers/default_pattern.bloc.dart';
import 'package:oudg_flutter/controllers/global_settings.bloc.dart';
import 'package:oudg_flutter/controllers/navigation.bloc.provider.dart';
import 'package:oudg_flutter/views/android/widgets/menu.button.dart';
import 'package:provider/provider.dart';

class MyMenu extends StatefulWidget {
  @override
  _MyMenuState createState() => _MyMenuState();
}

class _MyMenuState extends State<MyMenu> {
  @override
  Widget build(BuildContext context) {
    var defPat = MyDefaultPattern();
    GlobalSettings gblSet = GlobalSettings();
    var navFrag = Provider.of<MyNavigation>(context);

    return Column(
      children: [
        Container(
          height: gblSet.fragHeight,
          margin: EdgeInsets.only(
            left: gblSet.sizeW(5),
            right: gblSet.sizeW(5),
          ),
          child: ListView(
            children: [
              MyMenuButton(
                textButton: "Sua Casta",
                assetButton: defPat.assBgMenu,
                func: () => {navFrag.navTo(navFrag.getPage('pagCaste'))},
              ),
              MyMenuButton(
                textButton: "Tradutor Tribal",
                assetButton: defPat.assBgMenu,
                func: () => {navFrag.navTo(navFrag.getPage('pagTribal'))},
              ),
              MyMenuButton(
                textButton: "Registro de Nascimento",
                assetButton: defPat.assBgMenu,
                func: () => {navFrag.navTo(navFrag.getPage('pagRecord'))},
              ),
              MyMenuButton(
                textButton: "Teste de Conhecimentos",
                assetButton: defPat.assBgMenu,
                func: () => {navFrag.navTo(navFrag.getPage('pagQuiz'))},
              ),
              MyMenuButton(
                textButton: "Jogos da Taverna do Afiado",
                assetButton: defPat.assBgMenuOff,
                func: () => {navFrag.navTo(navFrag.getPage('pagTavern'))},
              ),
              MyMenuButton(
                textButton: "RPG",
                assetButton: defPat.assBgMenuOff,
                func: () => {navFrag.navTo(navFrag.getPage('pagRPG'))},
              ),
              MyMenuButton(
                textButton: "Loja",
                assetButton: defPat.assBgMenuOff,
                func: () => {navFrag.navTo(navFrag.getPage('pagStore'))},
              ),
            ],
          ),
        ),
      ],
    );
  }
}
