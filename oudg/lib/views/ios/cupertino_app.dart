import 'package:flutter/cupertino.dart';
import 'package:oudg_flutter/views/android/pages/splash.view.dart';
import 'package:oudg_flutter/views/ios/home.view.dart';

class MyCupertinoApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      title: "O Último dos Guardiões",
      debugShowCheckedModeBanner: false,
      theme: CupertinoThemeData(
        textTheme: CupertinoTextThemeData(
          textStyle: TextStyle(fontFamily: 'Papyrus'),
        ),
      ),
      home: MySplash(
        myHome: HomeView(),
      ),
    );
  }
}
