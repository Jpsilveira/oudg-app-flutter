import 'package:flutter/material.dart';
import 'package:oudg_flutter/controllers/default_pattern.bloc.dart';
import 'package:oudg_flutter/controllers/global_settings.bloc.dart';
import 'package:oudg_flutter/controllers/navigation.bloc.provider.dart';
import 'package:oudg_flutter/questions/questions_caste.dart';
import 'package:oudg_flutter/questions/questions_caste_conclusion.view.dart';
import 'package:oudg_flutter/questions/questions_conclusion.view.dart';
import 'package:oudg_flutter/questions/questions_json.dart';
import 'package:oudg_flutter/questions/questions_list.dart';
import 'package:oudg_flutter/questions/questions_structure.dart';
import 'package:oudg_flutter/views/android/widgets/navigation.button.dart';
import 'package:provider/provider.dart';

class Questions extends StatefulWidget {
  final int quizNumber;

  Questions({
    Key key,
    @required this.quizNumber,
  }) : super(key: key);

  _QuestionsState createState() => _QuestionsState();
}

class _QuestionsState extends State<Questions> {
  GlobalSettings gblSet = GlobalSettings();

  int _questionCurrent = 0;
  int _questionResult = 0;
  bool isFinished = false;

  // ### Temporário até obter o json do backend
  QuizStructure _quiz;
  List<QuestionsStructure> _questions;
  List<ResultStructure> _result;

  var defPat = MyDefaultPattern();

  _answer(int answer) {
    setState(() {
      _questions[_questionCurrent].questionAnswer = answer;
    });
  }

  _previousQuestion() {
    if (_questionCurrent > 0) {
      setState(() {
        _questionCurrent--;
      });
    }
  }

  _nextQuestion() {
    if (_questionCurrent + 1 < _questions.length) {
      setState(() {
        _questionCurrent++;
      });
    }
  }

  _goQuestion(int question) {
    if (question >= 0 && question < _questions.length) {
      setState(() {
        _questionCurrent = question;
      });
    }
  }

  _runResult() {
    if (widget.quizNumber == 1) {
      _questionResult = QuestionCaste().calculateResultCasteQuiz(
        quiz: _quiz,
        questions: _questions,
        result: _result,
      );
    } else {
      _questionResult = calculateResultQuiz();
    }
  }

  int calculateResultQuiz() {
    String resultAttribute = "";
    double quizResult = 0;
    int index = -1;

    _quiz.quizQuestions.forEach((q) {
      if (q.questionAnswer > -1) {
        _result.forEach((r) {
          if (r.resultAttribute == q.questionAttribute) {
            r.resultScore +=
                q.questionAnswersList[q.questionAnswer - 1].answerWeight;
          }
        });
      }
    });
    _result.forEach((r) {
      if (r.resultScore > quizResult) {
        resultAttribute = r.resultAttribute;
        quizResult = r.resultScore;
        print(r.resultAttribute);
        print(r.resultScore);
      }
    });

    index = -1;
    index = _result.indexWhere((r) => r.resultAttribute == resultAttribute);
    if (index < 0) {
      print("Não fez nenhum ponto.");
      index = 0;
    }
    return index;
  }

  @override
  void initState() {
    super.initState();

    _quiz = QuestionsJson().myQuestionsJson(widget.quizNumber);
    _result = QuestionsJson().myResultJson(widget.quizNumber);
  }

  @override
  Widget build(BuildContext context) {
    var defPat = MyDefaultPattern();
    var navFrag = Provider.of<MyNavigation>(context);
    GlobalSettings gblSet = GlobalSettings();

    _questions = _quiz.quizQuestions;
    _questions.sort((a, b) => a.questionOrder.compareTo(b.questionOrder));

    return Column(
      children: [
        isFinished
            ? widget.quizNumber == 1
                ? QuestionsCasteResult(
                    myTitle: _result[_questionResult].resultTitle,
                    myText: _result[_questionResult].resultText,
                    myCaste: _result[_questionResult].resultAttribute,
                  )
                : QuestionsResult(
                    myTitle: _result[_questionResult].resultTitle,
                    myText: _result[_questionResult].resultText,
                    myImage: _result[_questionResult].resultImage,
                    myScore: _result[_questionResult].resultScore,
                  )
            : QuestionsList(
                quiz: _quiz,
                questionCurrent: _questionCurrent,
                questionAnswer: _questions[_questionCurrent].questionAnswer,
                answerFunction: _answer,
              ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            MyNavButton(
              textAlign: defPat.aliRight,
              textButton: _questionCurrent == 0 ? "Voltar ao Menu" : "Anterior",
              assetButton: defPat.assBack,
              func: () {
                if (_questionCurrent == 0 || isFinished) {
                  navFrag.navTo(navFrag.getPage('pagMenu'));
                } else {
                  _previousQuestion();
                }
              },
            ),
            isFinished
                ? SizedBox()
                : MyNavButton(
                    textAlign: defPat.aliLeft,
                    textButton: _questionCurrent == _questions.length - 1
                        ? "Concluir"
                        : "Próxima",
                    assetButton: defPat.assForward,
                    func: () {
                      print(_questions[_questionCurrent].questionAnswer);
                      if (_questions[_questionCurrent].questionAnswer != -1) {
                        if (_questionCurrent == _questions.length - 1) {
                          setState(() {
                            _runResult();
                            isFinished = true;
                          });
                        } else {
                          _nextQuestion();
                        }
                      } else {
                        Scaffold.of(context).showSnackBar(
                          SnackBar(
                            margin: EdgeInsets.only(
                              bottom: gblSet.sizeW(23),
                              left: gblSet.sizeW(10),
                              right: gblSet.sizeW(10),
                            ),
                            elevation: 6.0,
                            behavior: SnackBarBehavior.floating,
                            backgroundColor: Colors.brown.withOpacity(0.7),
                            duration: Duration(seconds: 1),
                            content: Text(
                              "Selecione uma opção.",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: gblSet.ftsNormal,
                              ),
                            ),
                          ),
                        );
                      }
                    },
                  ),
          ],
        ),
      ],
    );
  }
}
