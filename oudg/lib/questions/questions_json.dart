import 'package:oudg_flutter/questions/questions_structure.dart';

class QuestionsJson {
  QuizStructure myQuestionsJson(int quiz) {
    if (quiz == 1) {
      return QuizStructure(
        quizId: 1,
        quizTitle: "Sua Casta",
        quizQuestions: [
          QuestionsStructure(
            questionId: 1,
            questionOrder: 1,
            questionWeight: 1,
            questionAttribute: "CRG",
            questionText:
                "Quando você entra em um combate, prefere lutar com os inimigos que você sabe que pode derrotar. Afinal, cada um sabe do que pode dar conta.",
            questionAnswersList: [
              AnswerStructure(
                  answerId: 1,
                  answerText: "Discordo Totalmente",
                  answerWeight: 0.25),
              AnswerStructure(
                  answerId: 2,
                  answerText: "Discordo em Parte",
                  answerWeight: 0.50),
              AnswerStructure(
                  answerId: 3, answerText: "Indiferente", answerWeight: 0.00),
              AnswerStructure(
                  answerId: 4,
                  answerText: "Concordo em Parte",
                  answerWeight: 0.75),
              AnswerStructure(
                  answerId: 5,
                  answerText: "Concordo Totalmente",
                  answerWeight: 1.00),
            ],
            questionAnswer: -1,
          ),
          QuestionsStructure(
            questionId: 2,
            questionOrder: 9,
            questionWeight: 1,
            questionAttribute: "CRG",
            questionText:
                "Você está sozinho e vê um inocente sendo atacado por uma horda de demônios. Seu dever é partir em defesa dele, independente do que acontecerá a você.",
            questionAnswersList: [
              AnswerStructure(
                  answerId: 1,
                  answerText: "Discordo Totalmente",
                  answerWeight: 0.25),
              AnswerStructure(
                  answerId: 2,
                  answerText: "Discordo em Parte",
                  answerWeight: 0.50),
              AnswerStructure(
                  answerId: 3, answerText: "Indiferente", answerWeight: 0.00),
              AnswerStructure(
                  answerId: 4,
                  answerText: "Concordo em Parte",
                  answerWeight: 0.75),
              AnswerStructure(
                  answerId: 5,
                  answerText: "Concordo Totalmente",
                  answerWeight: 1.00),
            ],
            questionAnswer: -1,
          ),
          QuestionsStructure(
            questionId: 3,
            questionOrder: 2,
            questionWeight: 1,
            questionAttribute: "DST",
            questionText:
                "Quanto mais rápida for a luta, menos eu me ferirei. Agilidade é fundamental.",
            questionAnswersList: [
              AnswerStructure(
                  answerId: 1,
                  answerText: "Discordo Totalmente",
                  answerWeight: 0.25),
              AnswerStructure(
                  answerId: 2,
                  answerText: "Discordo em Parte",
                  answerWeight: 0.50),
              AnswerStructure(
                  answerId: 3, answerText: "Indiferente", answerWeight: 0.00),
              AnswerStructure(
                  answerId: 4,
                  answerText: "Concordo em Parte",
                  answerWeight: 0.75),
              AnswerStructure(
                  answerId: 5,
                  answerText: "Concordo Totalmente",
                  answerWeight: 1.00),
            ],
            questionAnswer: -1,
          ),
          QuestionsStructure(
            questionId: 4,
            questionOrder: 10,
            questionWeight: 1,
            questionAttribute: "DST",
            questionText:
                "A velocidade é o atributo principal em um ataque. Precisamos chegar rápido no inimigo.",
            questionAnswersList: [
              AnswerStructure(
                  answerId: 1,
                  answerText: "Discordo Totalmente",
                  answerWeight: 0.25),
              AnswerStructure(
                  answerId: 2,
                  answerText: "Discordo em Parte",
                  answerWeight: 0.50),
              AnswerStructure(
                  answerId: 3, answerText: "Indiferente", answerWeight: 0.00),
              AnswerStructure(
                  answerId: 4,
                  answerText: "Concordo em Parte",
                  answerWeight: 0.75),
              AnswerStructure(
                  answerId: 5,
                  answerText: "Concordo Totalmente",
                  answerWeight: 1.00),
            ],
            questionAnswer: -1,
          ),
          QuestionsStructure(
            questionId: 5,
            questionOrder: 11,
            questionWeight: 1,
            questionAttribute: "INT",
            questionText:
                "É melhor pensar antes de agir. Nem sempre um ataque rápido é a resposta.",
            questionAnswersList: [
              AnswerStructure(
                  answerId: 1,
                  answerText: "Discordo Totalmente",
                  answerWeight: 0.25),
              AnswerStructure(
                  answerId: 2,
                  answerText: "Discordo em Parte",
                  answerWeight: 0.50),
              AnswerStructure(
                  answerId: 3, answerText: "Indiferente", answerWeight: 0.00),
              AnswerStructure(
                  answerId: 4,
                  answerText: "Concordo em Parte",
                  answerWeight: 0.75),
              AnswerStructure(
                  answerId: 5,
                  answerText: "Concordo Totalmente",
                  answerWeight: 1.00),
            ],
            questionAnswer: -1,
          ),
          QuestionsStructure(
            questionId: 6,
            questionOrder: 3,
            questionWeight: 1,
            questionAttribute: "INT",
            questionText: "O guerreiro sábio sempre sobrepuja o forte.",
            questionAnswersList: [
              AnswerStructure(
                  answerId: 1,
                  answerText: "Discordo Totalmente",
                  answerWeight: 0.25),
              AnswerStructure(
                  answerId: 2,
                  answerText: "Discordo em Parte",
                  answerWeight: 0.50),
              AnswerStructure(
                  answerId: 3, answerText: "Indiferente", answerWeight: 0.00),
              AnswerStructure(
                  answerId: 4,
                  answerText: "Concordo em Parte",
                  answerWeight: 0.75),
              AnswerStructure(
                  answerId: 5,
                  answerText: "Concordo Totalmente",
                  answerWeight: 1.00),
            ],
            questionAnswer: -1,
          ),
          QuestionsStructure(
            questionId: 7,
            questionOrder: 4,
            questionWeight: 1,
            questionAttribute: "EST",
            questionText:
                "Numa batalha, vence aquele que se preparou mais para ela, não o exército com mais guerreiros.",
            questionAnswersList: [
              AnswerStructure(
                  answerId: 1,
                  answerText: "Discordo Totalmente",
                  answerWeight: 0.25),
              AnswerStructure(
                  answerId: 2,
                  answerText: "Discordo em Parte",
                  answerWeight: 0.50),
              AnswerStructure(
                  answerId: 3, answerText: "Indiferente", answerWeight: 0.00),
              AnswerStructure(
                  answerId: 4,
                  answerText: "Concordo em Parte",
                  answerWeight: 0.75),
              AnswerStructure(
                  answerId: 5,
                  answerText: "Concordo Totalmente",
                  answerWeight: 1.00),
            ],
            questionAnswer: -1,
          ),
          QuestionsStructure(
            questionId: 8,
            questionOrder: 12,
            questionWeight: 1,
            questionAttribute: "EST",
            questionText:
                "No campo de batalha, todos os detalhes são importantes: Clima, terreno, horário... Nem sempre um ataque rápido é a solução ideal.",
            questionAnswersList: [
              AnswerStructure(
                  answerId: 1,
                  answerText: "Discordo Totalmente",
                  answerWeight: 0.25),
              AnswerStructure(
                  answerId: 2,
                  answerText: "Discordo em Parte",
                  answerWeight: 0.50),
              AnswerStructure(
                  answerId: 3, answerText: "Indiferente", answerWeight: 0.00),
              AnswerStructure(
                  answerId: 4,
                  answerText: "Concordo em Parte",
                  answerWeight: 0.75),
              AnswerStructure(
                  answerId: 5,
                  answerText: "Concordo Totalmente",
                  answerWeight: 1.00),
            ],
            questionAnswer: -1,
          ),
          QuestionsStructure(
            questionId: 9,
            questionOrder: 13,
            questionWeight: 1,
            questionAttribute: "FRC",
            questionText:
                "Contra força não há resistência. De que adianta ficar pensando, quanto o inimigo é muito mais forte.",
            questionAnswersList: [
              AnswerStructure(
                  answerId: 1,
                  answerText: "Discordo Totalmente",
                  answerWeight: 1.00),
              AnswerStructure(
                  answerId: 2,
                  answerText: "Discordo em Parte",
                  answerWeight: 0.75),
              AnswerStructure(
                  answerId: 3, answerText: "Indiferente", answerWeight: 0.00),
              AnswerStructure(
                  answerId: 4,
                  answerText: "Concordo em Parte",
                  answerWeight: 0.50),
              AnswerStructure(
                  answerId: 5,
                  answerText: "Concordo Totalmente",
                  answerWeight: 0.25),
            ],
            questionAnswer: -1,
          ),
          QuestionsStructure(
            questionId: 10,
            questionOrder: 5,
            questionWeight: 1,
            questionAttribute: "FRC",
            questionText: "A força de um guerreiro é sua principal habilidade.",
            questionAnswersList: [
              AnswerStructure(
                  answerId: 1,
                  answerText: "Discordo Totalmente",
                  answerWeight: 0.25),
              AnswerStructure(
                  answerId: 2,
                  answerText: "Discordo em Parte",
                  answerWeight: 0.50),
              AnswerStructure(
                  answerId: 3, answerText: "Indiferente", answerWeight: 0.00),
              AnswerStructure(
                  answerId: 4,
                  answerText: "Concordo em Parte",
                  answerWeight: 0.75),
              AnswerStructure(
                  answerId: 5,
                  answerText: "Concordo Totalmente",
                  answerWeight: 1.00),
            ],
            questionAnswer: -1,
          ),
          QuestionsStructure(
            questionId: 11,
            questionOrder: 6,
            questionWeight: 1,
            questionAttribute: "HNR",
            questionText:
                "O importante é vencer a batalha, não importa a que custo. A vida de muitos se sobrepõe a de apenas um indivíduo.",
            questionAnswersList: [
              AnswerStructure(
                  answerId: 1,
                  answerText: "Discordo Totalmente",
                  answerWeight: 1.00),
              AnswerStructure(
                  answerId: 2,
                  answerText: "Discordo em Parte",
                  answerWeight: 0.75),
              AnswerStructure(
                  answerId: 3, answerText: "Indiferente", answerWeight: 0.00),
              AnswerStructure(
                  answerId: 4,
                  answerText: "Concordo em Parte",
                  answerWeight: 0.50),
              AnswerStructure(
                  answerId: 5,
                  answerText: "Concordo Totalmente",
                  answerWeight: 0.25),
            ],
            questionAnswer: -1,
          ),
          QuestionsStructure(
            questionId: 12,
            questionOrder: 14,
            questionWeight: 1,
            questionAttribute: "HNR",
            questionText:
                "Um grande guerreiro luta limpo, não se vale de enganos ou artimanhas para vencer a batalha. Vencer nem sempre é o mais importante.",
            questionAnswersList: [
              AnswerStructure(
                  answerId: 1,
                  answerText: "Discordo Totalmente",
                  answerWeight: 0.25),
              AnswerStructure(
                  answerId: 2,
                  answerText: "Discordo em Parte",
                  answerWeight: 0.50),
              AnswerStructure(
                  answerId: 3, answerText: "Indiferente", answerWeight: 0.00),
              AnswerStructure(
                  answerId: 4,
                  answerText: "Concordo em Parte",
                  answerWeight: 0.75),
              AnswerStructure(
                  answerId: 5,
                  answerText: "Concordo Totalmente",
                  answerWeight: 1.00),
            ],
            questionAnswer: -1,
          ),
          QuestionsStructure(
            questionId: 13,
            questionOrder: 7,
            questionWeight: 1,
            questionAttribute: "LAD",
            questionText:
                "Na linha de frente, tudo que o guerreiro tem são seus companheiros. Ou todos voltam ou não volta ninguém.",
            questionAnswersList: [
              AnswerStructure(
                  answerId: 1,
                  answerText: "Discordo Totalmente",
                  answerWeight: 0.25),
              AnswerStructure(
                  answerId: 2,
                  answerText: "Discordo em Parte",
                  answerWeight: 0.50),
              AnswerStructure(
                  answerId: 3, answerText: "Indiferente", answerWeight: 0.00),
              AnswerStructure(
                  answerId: 4,
                  answerText: "Concordo em Parte",
                  answerWeight: 0.75),
              AnswerStructure(
                  answerId: 5,
                  answerText: "Concordo Totalmente",
                  answerWeight: 1.00),
            ],
            questionAnswer: -1,
          ),
          QuestionsStructure(
            questionId: 14,
            questionOrder: 15,
            questionWeight: 1,
            questionAttribute: "LAD",
            questionText:
                "Um comandante é responsável por seus subordinados e sempre deve protegê-los. Mesmo que isso custe a sua vida.",
            questionAnswersList: [
              AnswerStructure(
                  answerId: 1,
                  answerText: "Discordo Totalmente",
                  answerWeight: 0.25),
              AnswerStructure(
                  answerId: 2,
                  answerText: "Discordo em Parte",
                  answerWeight: 0.50),
              AnswerStructure(
                  answerId: 3, answerText: "Indiferente", answerWeight: 0.00),
              AnswerStructure(
                  answerId: 4,
                  answerText: "Concordo em Parte",
                  answerWeight: 0.75),
              AnswerStructure(
                  answerId: 5,
                  answerText: "Concordo Totalmente",
                  answerWeight: 1.00),
            ],
            questionAnswer: -1,
          ),
          QuestionsStructure(
            questionId: 15,
            questionOrder: 8,
            questionWeight: 1,
            questionAttribute: "RES",
            questionText:
                "Não importa a quantidade do inimigo. A garra do guerreiro é o que decide a batalha.",
            questionAnswersList: [
              AnswerStructure(
                  answerId: 1,
                  answerText: "Discordo Totalmente",
                  answerWeight: 0.25),
              AnswerStructure(
                  answerId: 2,
                  answerText: "Discordo em Parte",
                  answerWeight: 0.50),
              AnswerStructure(
                  answerId: 3, answerText: "Indiferente", answerWeight: 0.00),
              AnswerStructure(
                  answerId: 4,
                  answerText: "Concordo em Parte",
                  answerWeight: 0.75),
              AnswerStructure(
                  answerId: 5,
                  answerText: "Concordo Totalmente",
                  answerWeight: 1.00),
            ],
            questionAnswer: -1,
          ),
          QuestionsStructure(
            questionId: 16,
            questionOrder: 16,
            questionWeight: 1,
            questionAttribute: "RES",
            questionText:
                "Não existe tarefa impossível para aquele que acredita em si.",
            questionAnswersList: [
              AnswerStructure(
                  answerId: 1,
                  answerText: "Discordo Totalmente",
                  answerWeight: 0.25),
              AnswerStructure(
                  answerId: 2,
                  answerText: "Discordo em Parte",
                  answerWeight: 0.50),
              AnswerStructure(
                  answerId: 3, answerText: "Indiferente", answerWeight: 0.00),
              AnswerStructure(
                  answerId: 4,
                  answerText: "Concordo em Parte",
                  answerWeight: 0.75),
              AnswerStructure(
                  answerId: 5,
                  answerText: "Concordo Totalmente",
                  answerWeight: 1.00),
            ],
            questionAnswer: -1,
          ),
        ],
      );
    } else if (quiz == 2) {
      return QuizStructure(
        quizId: 2,
        quizTitle: "Teste de Conhecimentos",
        quizQuestions: [
          QuestionsStructure(
            questionId: 1,
            questionOrder: 1,
            questionWeight: 1,
            questionAttribute: "TOT",
            questionText: "Qual o lema dos Guardiões?",
            questionAnswersList: [
              AnswerStructure(
                  answerId: 1,
                  answerText: "Força e agilidade!",
                  answerWeight: 0),
              AnswerStructure(
                  answerId: 2,
                  answerText: "Coragem e Força!",
                  answerWeight: 0.00),
              AnswerStructure(
                  answerId: 3, answerText: "Atacar!", answerWeight: 0.00),
              AnswerStructure(
                  answerId: 4,
                  answerText: "Coragem e Honra!",
                  answerWeight: 1.00),
              AnswerStructure(
                  answerId: 5,
                  answerText: "Lutar, lutar, lutar!",
                  answerWeight: 0),
            ],
            questionAnswer: -1,
          ),
        ],
      );
    }
  }

  List<ResultStructure> myResultJson(int quiz) {
    if (quiz == 1) {
      return [
        ResultStructure(
            resultAttribute: "AVT",
            resultScore: 0,
            resultTitle: "Avantes",
            resultText:
                """Parabéns, você agora é um guerreiro da casta dos Avantes. Você é a linha de frente no ataque contra às forças demoníacas. Faça o maldito Balkatar sentir o peso do nosso braço!""",
            resultImage: "assets/selo_avantes.png"),
        ResultStructure(
            resultAttribute: "BIO",
            resultScore: 0,
            resultTitle: "Biontes",
            resultText:
                """Parabéns, você agora é um guerreiro da casta dos Biontes. Salve seus irmãos de guerra e nos proteja dos ataques aéreos. Seu poder de cura é fundamental para nosso exército. Com ele, somos fortes... Somos Invencíveis.""",
            resultImage: "assets/selo_avantes.png"),
        ResultStructure(
            resultAttribute: "GRD",
            resultScore: 0,
            resultTitle: "Guardiões",
            resultText:
                """Parabéns, você agora é um guerreiro da casta dos Guardiões. Sua nobre missão é defender seus irmãos em armas e proteger os inocentes contra a tirania dos exércitos de demônios do terrível Balkatar.""",
            resultImage: "assets/selo_guardioes.png"),
        ResultStructure(
            resultAttribute: "MTL",
            resultScore: 0,
            resultTitle: "Mentales",
            resultText:
                """Parabéns, você agora é um guerreiro da casta dos Mentales. Não deixe os inimigos se aproximarem, ataque com sua poderosa magia e mostre aos demônios quem realmente tem o poder.""",
            resultImage: "assets/selo_mentales.png"),
      ];
    } else if (quiz == 2) {
      return [
        ResultStructure(
            resultAttribute: "TOT",
            resultScore: 0,
            resultTitle: "Parabéns!",
            resultText: """Sua pontuação foi de: """,
            resultImage: "assets/like.png"),
      ];
    }
  }
}
