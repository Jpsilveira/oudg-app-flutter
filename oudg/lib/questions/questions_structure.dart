import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class QuizStructure {
  final int quizId;
  final String quizTitle;
  final List<QuestionsStructure> quizQuestions;

  QuizStructure({
    @required this.quizId,
    @required this.quizTitle,
    @required this.quizQuestions,
  });
}

class QuestionsStructure {
  final int questionId;
  final int questionOrder;
  final double questionWeight;
  final String questionAttribute;
  final String questionText;
  final List<AnswerStructure> questionAnswersList;
  int questionAnswer;

  QuestionsStructure({
    @required this.questionId,
    @required this.questionOrder,
    @required this.questionWeight,
    @required this.questionAttribute,
    @required this.questionText,
    @required this.questionAnswersList,
    @required this.questionAnswer,
  });
}

class AnswerStructure {
  final int answerId;
  final String answerText;
  final double answerWeight;

  AnswerStructure({
    @required this.answerId,
    @required this.answerText,
    @required this.answerWeight,
  });
}

class QuizAnswer {
  int quizId;
  List<QuestionAnswer> questionAnswerList;

  QuizAnswer({
    @required this.quizId,
    @required this.questionAnswerList,
  });
}

class QuestionAnswer {
  int questionId;
  int questionAnswer;
  double questionWeight;
  String questionAttribute;

  QuestionAnswer({
    @required this.questionId,
    @required this.questionAnswer,
    @required this.questionWeight,
    @required this.questionAttribute,
  });
}

class ResultStructure {
  final String resultAttribute;
  double resultScore;
  final String resultTitle;
  final String resultText;
  final String resultImage;

  ResultStructure({
    @required this.resultAttribute,
    @required this.resultScore,
    @required this.resultTitle,
    @required this.resultText,
    @required this.resultImage,
  });
}
