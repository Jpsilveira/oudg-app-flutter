import 'package:flutter/material.dart';
import 'package:oudg_flutter/controllers/default_pattern.bloc.dart';
import 'package:oudg_flutter/controllers/global_settings.bloc.dart';
import 'package:oudg_flutter/views/android/widgets/questions.button.dart';

import 'questions_structure.dart';

class QuestionsList extends StatelessWidget {
  final QuizStructure quiz;
  final int questionCurrent;
  final int questionAnswer;
  final Function(int) answerFunction;

  QuestionsList({
    @required this.quiz,
    @required this.questionCurrent,
    @required this.questionAnswer,
    @required this.answerFunction,
  });

  @override
  Widget build(BuildContext context) {
    var defPat = MyDefaultPattern();
    GlobalSettings gblSet = GlobalSettings();
    List<QuestionsStructure> questions = quiz.quizQuestions;

    List<AnswerStructure> answers = questionCurrent < questions.length
        ? questions[questionCurrent].questionAnswersList
        : null;

    return Container(
      height: gblSet.papyrusBottom,
      width: gblSet.fragWidth,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(
              left: gblSet.sizeW(10),
              right: gblSet.sizeW(10),
              bottom: gblSet.sizeH(10),
              top: gblSet.sizeH(20),
            ),
            child: Text(
              quiz.quizTitle,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: gblSet.ftsTitle,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Text(
            questions[questionCurrent].questionText,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: gblSet.ftsNormal,
            ),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(
                top: 15,
                bottom: 15,
              ),
              alignment: Alignment.bottomCenter,
              child: ListView(
                shrinkWrap: true,
                // padding: EdgeInsets.only(
                //   top: 30,
                //   bottom: 20,
                //   left: 0,
                //   right: 0,
                // ),
                children: [
                  ...answers.map((resp) => MyQuestButton(
                        textAlign: defPat.aliRight,
                        textButton: resp.answerText,
                        assetButtonOn: defPat.assSelecOn,
                        assetButtonOff: defPat.assSelecOff,
                        onOff: (resp.answerId == questionAnswer),
                        func: () {
                          answerFunction(resp.answerId);
                        },
                      )),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
