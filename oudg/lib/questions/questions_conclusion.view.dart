import 'package:flutter/material.dart';
import 'package:oudg_flutter/controllers/default_pattern.bloc.dart';
import 'package:oudg_flutter/controllers/global_context.bloc.dart';
import 'package:oudg_flutter/controllers/global_settings.bloc.dart';
import 'package:provider/provider.dart';

class QuestionsResult extends StatelessWidget {
  String myTitle;
  String myText;
  String myImage;
  double myScore;

  QuestionsResult({
    this.myTitle,
    this.myText,
    this.myImage,
    this.myScore,
  });

  @override
  Widget build(BuildContext context) {
    var defPat = MyDefaultPattern();
    var gloCont = Provider.of<MyGlobalContext>(context);
    GlobalSettings gblSet = GlobalSettings();

    return Column(
      children: [
        Container(
          width: gblSet.fragWidth,
          height: gblSet.papyrusBottom,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(defPat.assPapyrus),
              fit: BoxFit.fill,
            ),
          ),
          child: Container(
            // height: gblSet.papyrusBottom,
            padding: EdgeInsets.all(gblSet.sizeH(20)),
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                Text(
                  myScore == 0 ? "Tente novamente!" : myTitle,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: gblSet.ftsTitle,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(
                  height: gblSet.sizeH(10),
                ),
                Text(
                  myScore == 0 ? "Infelizmente você não pontuou." : myText,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: gblSet.ftsNormal,
                  ),
                ),
                Text(
                  myScore == 0 ? "" : myScore.toStringAsPrecision(3),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: gblSet.ftsTitle,
                  ),
                ),
                SizedBox(
                  height: gblSet.sizeH(50),
                ),
                SizedBox(
                  height: gblSet.sizeH(100),
                  width: gblSet.sizeW(100),
                  child: myScore == 0
                      ? SizedBox()
                      : Image.asset(
                          myImage,
                        ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
