import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Caste {
  final String casteKey;
  final String casteName;

  Caste({
    @required this.casteKey,
    @required this.casteName,
  });
}

class Attribute {
  final String attributeKey;
  final String attributeName;

  Attribute({
    @required this.attributeKey,
    @required this.attributeName,
  });
}

class AttributeValue {
  final String attributeKey;
  double attributeValue;

  AttributeValue({
    @required this.attributeKey,
    @required this.attributeValue,
  });
}

class CasteAttributes {
  final String casteKey;
  final List<AttributeValue> attributeList;

  CasteAttributes({
    @required this.casteKey,
    @required this.attributeList,
  });
}
