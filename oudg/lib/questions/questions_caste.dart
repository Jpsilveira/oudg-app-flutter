import 'package:oudg_flutter/controllers/global_settings.bloc.dart';
import 'package:oudg_flutter/questions/questions_caste_json.dart';
import 'package:oudg_flutter/questions/questions_caste_structure.dart';
import 'package:oudg_flutter/questions/questions_structure.dart';

class QuestionCaste {
  // QuestionCaste();

  int calculateResultCasteQuiz({
    QuizStructure quiz,
    List<QuestionsStructure> questions,
    List<ResultStructure> result,
  }) {
    int index = -1;
    List<AttributeValue> attributeProp = [];
    double quizResult = 0;
    String resultAttribute = "";
    List<CasteAttributes> _attributes =
        QuestionsCasteJson().myCastAttributesJson();
    GlobalSettings gblSet = GlobalSettings();

    _attributes.forEach((a1) {
      a1.attributeList.forEach((l1) {
        index =
            attributeProp.indexWhere((i) => i.attributeKey == l1.attributeKey);
        if (index < 0) {
          attributeProp.add(AttributeValue(
              attributeKey: l1.attributeKey,
              attributeValue: l1.attributeValue));
        } else {
          attributeProp[index].attributeValue += l1.attributeValue;
        }
      });
    });

    quiz.quizQuestions.forEach((q) {
      if (q.questionAnswer > -1) {
        _attributes.forEach((a) {
          a.attributeList.forEach((l) {
            if (l.attributeKey == q.questionAttribute) {
              result.forEach((r) {
                if (r.resultAttribute == a.casteKey) {
                  index = -1;
                  index = attributeProp
                      .indexWhere((i) => i.attributeKey == l.attributeKey);
                  if (index < 0) {
                    print("Erro de indexação");
                  } else {
                    r.resultScore += (q.questionWeight *
                        q.questionAnswersList[q.questionAnswer - 1]
                            .answerWeight *
                        (l.attributeValue /
                            attributeProp[index].attributeValue));
                    r.resultScore = gblSet.round(r.resultScore, 2);
                  }
                }
              });
            }
          });
        });
      }
    });

    result.forEach((r) {
      if (r.resultScore > quizResult) {
        resultAttribute = r.resultAttribute;
        quizResult = r.resultScore;
        print(r.resultAttribute);
        print(r.resultScore);
      }
    });

    index = -1;
    index = result.indexWhere((r) => r.resultAttribute == resultAttribute);
    if (index < 0) {
      print("Erro de indexação");
    }
    return index;
  }
}
