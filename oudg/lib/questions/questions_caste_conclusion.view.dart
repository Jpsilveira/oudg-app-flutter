import 'package:flutter/material.dart';
import 'package:oudg_flutter/controllers/default_pattern.bloc.dart';
import 'package:oudg_flutter/controllers/global_context.bloc.dart';
import 'package:oudg_flutter/controllers/global_settings.bloc.dart';
import 'package:provider/provider.dart';

class QuestionsCasteResult extends StatelessWidget {
  String myTitle;
  String myText;
  String myCaste;

  QuestionsCasteResult({
    this.myTitle,
    this.myText,
    this.myCaste,
  });

  @override
  Widget build(BuildContext context) {
    var defPat = MyDefaultPattern();
    var gloCont = Provider.of<MyGlobalContext>(context);
    GlobalSettings gblSet = GlobalSettings();
    gloCont.setUserCaste(myCaste);

    return Column(
      children: [
        Container(
          width: gblSet.fragWidth,
          height: gblSet.papyrusBottom,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(defPat.assPapyrus),
              fit: BoxFit.fill,
            ),
          ),
          child: Container(
            // height: gblSet.papyrusBottom,
            padding: EdgeInsets.all(gblSet.sizeH(20)),
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                Text(
                  myTitle,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: gblSet.ftsTitle,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(
                  height: gblSet.sizeH(10),
                ),
                Text(
                  myText,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: gblSet.ftsNormal,
                  ),
                ),
                SizedBox(
                  height: gblSet.sizeH(80),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: gblSet.sealH,
                      width: gblSet.sealW,
                      child: Image.asset(
                        gloCont.getUserCasteSeal(),
                      ),
                    ),
                    SizedBox(
                      width: gblSet.sizeW(10),
                    ),
                    Text(
                      myTitle.toUpperCase(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: gblSet.ftsNormal,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
