import 'package:oudg_flutter/questions/questions_caste_structure.dart';

class QuestionsCasteJson {
  List<CasteAttributes> myCastAttributesJson() {
    return [
      CasteAttributes(
        casteKey: "AVT",
        attributeList: [
          AttributeValue(attributeKey: "CRG", attributeValue: 8),
          AttributeValue(attributeKey: "DST", attributeValue: 5),
          AttributeValue(attributeKey: "INT", attributeValue: 6),
          AttributeValue(attributeKey: "EST", attributeValue: 7),
          AttributeValue(attributeKey: "FRC", attributeValue: 10),
          AttributeValue(attributeKey: "HNR", attributeValue: 4),
          AttributeValue(attributeKey: "LAD", attributeValue: 3),
          AttributeValue(attributeKey: "RES", attributeValue: 9),
        ],
      ),
      CasteAttributes(
        casteKey: "BIO",
        attributeList: [
          AttributeValue(attributeKey: "CRG", attributeValue: 4),
          AttributeValue(attributeKey: "DST", attributeValue: 8),
          AttributeValue(attributeKey: "INT", attributeValue: 10),
          AttributeValue(attributeKey: "EST", attributeValue: 9),
          AttributeValue(attributeKey: "FRC", attributeValue: 3),
          AttributeValue(attributeKey: "HNR", attributeValue: 5),
          AttributeValue(attributeKey: "LAD", attributeValue: 6),
          AttributeValue(attributeKey: "RES", attributeValue: 7),
        ],
      ),
      CasteAttributes(
        casteKey: "GRD",
        attributeList: [
          AttributeValue(attributeKey: "CRG", attributeValue: 6),
          AttributeValue(attributeKey: "DST", attributeValue: 3),
          AttributeValue(attributeKey: "INT", attributeValue: 5),
          AttributeValue(attributeKey: "EST", attributeValue: 4),
          AttributeValue(attributeKey: "FRC", attributeValue: 9),
          AttributeValue(attributeKey: "HNR", attributeValue: 7),
          AttributeValue(attributeKey: "LAD", attributeValue: 8),
          AttributeValue(attributeKey: "RES", attributeValue: 10),
        ],
      ),
      CasteAttributes(
        casteKey: "MTL",
        attributeList: [
          AttributeValue(attributeKey: "CRG", attributeValue: 7),
          AttributeValue(attributeKey: "DST", attributeValue: 10),
          AttributeValue(attributeKey: "INT", attributeValue: 9),
          AttributeValue(attributeKey: "EST", attributeValue: 8),
          AttributeValue(attributeKey: "FRC", attributeValue: 4),
          AttributeValue(attributeKey: "HNR", attributeValue: 6),
          AttributeValue(attributeKey: "LAD", attributeValue: 5),
          AttributeValue(attributeKey: "RES", attributeValue: 3),
        ],
      ),
    ];
  }
}
