import 'dart:io' show Platform;

import 'package:flutter/material.dart';
import 'package:oudg_flutter/views/ios/cupertino_app.dart';

import 'views/android/widgets/android_app.dart';

void main() async {
  if (Platform.isIOS) {
    runApp(MyCupertinoApp());
  } else {
    runApp(MyAndroidApp());
  }
}
